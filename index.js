const login = document.querySelector(".login");
const signUp = document.querySelector(".SignUp");

document.querySelector("input[value=\"Login\"").addEventListener("click", showLogin);
document.querySelector("input[value=\"SignUp\"").addEventListener("click", showSignUp);

function showLogin() {
    login.style.display = "flex";
    signUp.style.display = "none";
}

function showSignUp() {
    login.style.display = "none";
    signUp.style.display = "flex";
}


// validating Login Form
login.addEventListener("submit", (e) => {
    const email = document.getElementById("email");
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!email.value.match(mailformat)) {
        if (!document.getElementById("emailError")) {
            let errorMeassage = document.createElement("span");
            errorMeassage.innerText = "Invalid Email Format";
            errorMeassage.className = "errorMessage";
            errorMeassage.id = "emailError"
            email.insertAdjacentElement("afterend", errorMeassage);
        }
        e.preventDefault();
    }

    const password = document.getElementById("password");
    if (password.value.length < 8) {
        if (!document.getElementById("passwordError")) {
            let errorMeassage = document.createElement("span");
            errorMeassage.innerText = "Password should be greater than 8 characters";
            errorMeassage.className = "errorMessage";
            errorMeassage.id = "passwordError";
            password.insertAdjacentElement("afterend", errorMeassage);
        }
        e.preventDefault();
    }
})


//Validating SignUp
signUp.addEventListener("submit", (e) => {
    const name = document.getElementById("name");
    
    if (name.value.length === 0) {
        if (!document.getElementById("nameError")) {
            let errorMeassage = document.createElement("span");
            
            errorMeassage.innerText = "Name Required";
            errorMeassage.className = "errorMessage";
            errorMeassage.id = "nameError";
            name.insertAdjacentElement("afterend", errorMeassage);
        }
        e.preventDefault();
    }
    
    const email = document.getElementById("email2");
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!email.value.match(mailformat)) {
        if (!document.getElementById("emailError2")) {
            let errorMeassage = document.createElement("span");

            errorMeassage.innerText = "Invalid Email Format";
            errorMeassage.className = "errorMessage";
            errorMeassage.id = "emailError2"
            email.insertAdjacentElement("afterend", errorMeassage);
        }

        e.preventDefault();
    }

    const password = document.getElementById("password2");
  
    if (password.value.length < 8) {
        if (!document.getElementById("passwordError2")) {
            let errorMeassage = document.createElement("span");
  
            errorMeassage.innerText = "Password should be greater than 8 characters";
            errorMeassage.className = "errorMessage";
            errorMeassage.id = "passwordError2";
  
            password.insertAdjacentElement("afterend", errorMeassage);
        }
        e.preventDefault();
    }
})